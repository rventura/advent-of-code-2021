from functools import reduce

lines = [line.strip() for line in open("input/day10.txt")]
CHAR_PAIRS = {"(": ")", "[": "]", "{": "}", "<": ">"}
CORRUPT_CHARACTER_POINTS = {")": 3, "]": 57, "}": 1197, ">": 25137}
COMPLETION_POINTS = {")": 1, "]": 2, "}": 3, ">": 4}


def parse_line(l: str) -> tuple[list[str], str]:
    open_chunks = []
    for char in l:
        if char in CHAR_PAIRS.keys():
            open_chunks.append(char)
        else:
            if char == CHAR_PAIRS[open_chunks[-1]]:
                open_chunks.pop()
            else:
                return open_chunks, char
    return open_chunks, ""


def complete_line(left_open: list[str]) -> list[str]:
    char_pairs = {"(": ")", "[": "]", "{": "}", "<": ">"}
    comp = []
    for char in left_open[-1::-1]:
        comp.append(char_pairs[char])
    return comp


def completion_score(scores: list[int]) -> int:
    return reduce(lambda x, y: x * 5 + y, scores, 0)


# Parts 1 and 2
corrupt_chars = []
completions = []

for line in lines:
    chunks, corrupt_char = parse_line(line)
    if corrupt_char:
        corrupt_chars.append(corrupt_char)
    else:
        if chunks:
            completions.append(complete_line(chunks))

answer_1 = sum(CORRUPT_CHARACTER_POINTS[c] for c in corrupt_chars)
answer_2 = sorted(
    completion_score([COMPLETION_POINTS[c] for c in completion])
    for completion in completions
)[len(completions) // 2]
