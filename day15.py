"""Shamelessly uses redblobgames implementation of dijkstra's algorithm.

See https://www.redblobgames.com/pathfinding/a-star/implementation.html#python-dijkstra
"""

import heapq
import math
from typing import Iterator, Optional, TypeVar

import utils

T = TypeVar("T")
GridLocation = tuple[int, int]
Weight = int


class SquareGrid:
    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height

    def in_bounds(self, id_: GridLocation) -> bool:
        (x, y) = id_
        return 0 <= x < self.width and 0 <= y < self.height

    def neighbors(self, id_: GridLocation) -> Iterator[GridLocation]:
        (x, y) = id_
        neighbors = [(x + 1, y), (x - 1, y), (x, y - 1), (x, y + 1)]  # E W N S
        if (x + y) % 2 == 0:
            neighbors.reverse()  # S N W E
        return filter(self.in_bounds, neighbors)


class GridWithWeights(SquareGrid):
    def __init__(self, width: int, height: int):
        super().__init__(width, height)
        self.weights: dict[GridLocation, float] = {}

    def cost(self, to_node: GridLocation) -> float:
        return self.weights.get(to_node, 1)


class PriorityQueue:
    def __init__(self):
        self.elements: list[tuple[float, T]] = []

    def empty(self) -> bool:
        return not self.elements

    def put(self, item: T, priority: float):
        heapq.heappush(self.elements, (priority, item))

    def get(self) -> T:
        return heapq.heappop(self.elements)[1]


def dijkstra_search(graph: GridWithWeights, start: GridLocation, goal: GridLocation):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    came_from: dict[GridLocation, Optional[GridLocation]] = {}
    cost_so_far: dict[GridLocation, float] = {}
    came_from[start] = None
    cost_so_far[start] = 0

    while not frontier.empty():
        current: GridLocation = frontier.get()
        if current == goal:
            break
        for neigh in graph.neighbors(current):
            new_cost = cost_so_far[current] + graph.cost(neigh)
            if new_cost < cost_so_far.get(neigh, math.inf):
                cost_so_far[neigh] = new_cost
                priority = new_cost
                frontier.put(neigh, priority)
                came_from[neigh] = current

    return came_from, cost_so_far


def reconstruct_path(
    came_from: dict[GridLocation, GridLocation], start: GridLocation, goal: GridLocation
) -> list[GridLocation]:
    current: GridLocation = goal
    path: list[GridLocation] = []
    while current != start:
        path.append(current)
        current = came_from[current]
    return path


def increment_grid(data: list[list[Weight]]) -> list[list[Weight]]:
    return [[i % 9 + 1 for i in line] for line in data]


lines = [[int(s) for s in line.strip()] for line in open("input/day15.txt").readlines()]
all_grids = [utils.apply(increment_grid, lines, n) for n in range(9)]

# Part 1
start_point = (0, 0)
end_point = (99, 99)
small_w, small_h = len(lines[0]), len(lines)
small_grid = GridWithWeights(small_w, small_h)
small_grid.weights = {
    (i, j): lines[i][j] for i in range(small_w) for j in range(small_h)
}

history, _ = dijkstra_search(small_grid, start_point, end_point)
riskless_path = reconstruct_path(history, start_point, end_point)
answer_1 = sum(small_grid.weights[loc] for loc in riskless_path)

# Part 2
start_point = (0, 0)
end_point = (499, 499)
large_w, large_h = 5 * small_w, 5 * small_h
large_grid = GridWithWeights(large_w, large_h)
for hh in range(5):
    for ww in range(5):
        current_grid = all_grids[hh + ww]
        large_grid.weights.update(
            {
                (i + hh * small_h, j + ww * small_w): current_grid[i][j]
                for i in range(small_h)
                for j in range(small_w)
            }
        )
history, _ = dijkstra_search(large_grid, start_point, end_point)
riskless_path = reconstruct_path(history, start_point, end_point)
answer_2 = sum(large_grid.weights[loc] for loc in riskless_path)
