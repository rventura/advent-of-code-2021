from functools import reduce


def parse_input() -> tuple[list[list[str]], list[list[str]]]:
    lines = [line.strip().split(" | ") for line in open("input/day8.txt").readlines()]
    signal_patterns = []
    outputs = []
    for line in lines:
        signal_patterns.append(line[0].split(" "))
        outputs.append(line[1].split(" "))
    return signal_patterns, outputs


def get_signal_mapping(mixed_signals: list[str]) -> dict[str, str]:
    per_length: dict[int, list[set[str]]] = dict()
    for signal in mixed_signals:
        per_length.setdefault(len(signal), []).append(set(signal))

    ### Untangle each signal/segment association by deductive reasoning.
    mapping = dict()
    inter_6 = reduce(set.intersection, per_length[6])  # corresponds to abfg
    # a
    a_mapper = (per_length[3][0] - per_length[2][0]).pop()
    mapping[a_mapper] = "a"
    # b and d
    bd_candidates = per_length[4][0] - per_length[2][0]
    b_mapper = (bd_candidates & inter_6).pop()
    d_mapper = (bd_candidates - inter_6).pop()
    mapping[b_mapper] = "b"
    mapping[d_mapper] = "d"
    # c and f
    cf_candidates = per_length[4][0] - set([b_mapper, d_mapper])
    f_mapper = (cf_candidates & inter_6).pop()
    c_mapper = (cf_candidates - inter_6).pop()
    mapping[f_mapper] = "f"
    mapping[c_mapper] = "c"
    # g and e
    eg_candidates = set("abcdefg") - mapping.keys()
    g_mapper = (eg_candidates & inter_6).pop()
    e_mapper = (eg_candidates - inter_6).pop()
    mapping[g_mapper] = "g"
    mapping[e_mapper] = "e"
    return mapping


def decode_signal(num_string: list[str], mapping: dict) -> int:
    signal_to_digit = {
        "abcefg": "0",
        "cf": "1",
        "acdeg": "2",
        "acdfg": "3",
        "bcdf": "4",
        "abdfg": "5",
        "abdefg": "6",
        "acf": "7",
        "abcdefg": "8",
        "abcdfg": "9",
    }

    output = []
    for out_signal in num_string:
        dec_signal = [mapping[s] for s in out_signal]
        for dig_signal, digit in signal_to_digit.items():
            if sorted(dec_signal) == sorted(dig_signal):
                output.append(digit)
                break
    if len(output) != len(num_string):
        raise ValueError(
            "Could not transform string with signals {}".format(num_string)
        )
    return int("".join(output))


all_signal_patterns, all_outputs = parse_input()

# Part 1
simple_digits_counter = 0
for out in all_outputs:
    simple_digits_counter += sum(len(s) in [2, 3, 4, 7] for s in out)
answer_1 = simple_digits_counter

# Part 2
answer_2 = sum(
    decode_signal(output_signal, get_signal_mapping(signal_pattern))
    for signal_pattern, output_signal in zip(all_signal_patterns, all_outputs)
)
