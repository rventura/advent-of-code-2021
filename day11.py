from utils import cell_neighbors_ix

levels = [
    [int(i) for i in line.strip()] for line in open("input/day11.txt").readlines()
]
grid_shape = (len(levels), len(levels[0]))


class EnergyGrid:
    def __init__(self, initial_state: list[list[int]]):
        self.state = {}
        self.grid_shape = grid_shape
        for i, line in enumerate(initial_state):
            for j, lvl in enumerate(line):
                self.state[(i, j)] = lvl
        self.has_flashed: set[tuple[int, int]] = set()
        self.should_flash: set[tuple[int, int]] = set()
        self.flash_history: list[int] = []
        self.n_step = 0

    def step(self):
        self.n_step += 1
        self.state = {k: v + 1 for k, v in self.state.items()}
        while True:
            self.should_flash = {octo for octo, lvl in self.state.items() if lvl > 9}
            if self.should_flash:
                self.flash()
            else:
                break
        self.flash_history.append(len(self.has_flashed))
        self.has_flashed.clear()

    def flash(self):
        for ix in self.should_flash:
            self.has_flashed.add(ix)
            self.state[ix] = 0
            for neigh_ix in filter(
                lambda x: x not in self.has_flashed,
                cell_neighbors_ix(ix, self.grid_shape, True),
            ):
                self.state[neigh_ix] += 1


# Parts 1 and 2
octogrid = EnergyGrid(levels)
for _ in range(100):
    octogrid.step()

answer_1 = sum(octogrid.flash_history)

while True:
    octogrid.step()
    if octogrid.flash_history[-1] == grid_shape[0] * grid_shape[1]:
        answer_2 = octogrid.n_step
        break
