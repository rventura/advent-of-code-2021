import math
from typing import Callable

positions = [int(i) for i in open("input/day7.txt").read().split(",")]


def optimum(cost_function: Callable[[int], int]) -> float:
    min_fuel = math.inf
    for dest in range(min(positions), max(positions) + 1):
        n_fuel = sum(cost_function(abs(pos - dest)) for pos in positions)
        if n_fuel < min_fuel:
            min_fuel = n_fuel
    return min_fuel


answer_1 = optimum(lambda d: d)
answer_2 = optimum(lambda d: d * (d + 1) // 2)
