from collections import Counter

initial_state = [int(i) for i in open("input/day6.txt").read().split(",")]
state = dict(Counter(initial_state))

for day in range(1, 257):
    cycling_fishes = {6: state.get(0, 0)}
    new_fishes = {8: state.get(0, 0)}
    growing_fishes = {k - 1: v for k, v in state.items() if k >= 1}
    state = {
        k: sum(pool.get(k, 0) for pool in [cycling_fishes, new_fishes, growing_fishes])
        for k in range(9)
    }
    if day == 80:
        answer_1 = sum(state.values())
    if day == 256:
        answer_2 = sum(state.values())
