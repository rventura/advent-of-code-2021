report = [line.strip() for line in open("input/day3.txt").readlines()]


def col_n_ones(array: list[str], col: int):
    ith_bits = [r[col] for r in array]
    return len(list(filter(lambda b: b == "1", ith_bits)))


def filter_candidates(array: list[str], col: int, keep_most_common: bool):
    if col_n_ones(array, col) >= len(array) / 2:
        keep_with = "1" if keep_most_common else "0"
    else:
        keep_with = "0" if keep_most_common else "1"
    return [num for num in array if num[col] == keep_with]


# Part 1
n_bits = len(report[0])
most_common = [
    "1" if col_n_ones(report, i) > len(report) / 2 else "0" for i in range(n_bits)
]
least_common = ["0" if b == "1" else "1" for b in most_common]

GAMMA_RATE = int("0b" + "".join(most_common), 2)
EPSILON_RATE = int("0b" + "".join(least_common), 2)
answer_1 = GAMMA_RATE * EPSILON_RATE

# Part 2
oxygen_candidates = report.copy()
co2_candidates = report.copy()
ix = 0
while True:
    oxygen_candidates = filter_candidates(
        oxygen_candidates,
        ix,
        True,
    )
    ix += 1
    if len(oxygen_candidates) == 1:
        break
ix = 0
while True:
    co2_candidates = filter_candidates(
        co2_candidates,
        ix,
        False,
    )
    ix += 1
    if len(co2_candidates) == 1:
        break

oxygen_rate = int("0b" + oxygen_candidates.pop(), 2)
co2_rate = int("0b" + co2_candidates.pop(), 2)
answer_2 = oxygen_rate * co2_rate
