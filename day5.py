class Line:
    def __init__(self, p0: tuple[int, int], p1: tuple[int, int]):
        self.p0 = p0
        self.p1 = p1

    def is_straight(self):
        return self.p0[0] == self.p1[0] or self.p0[1] == self.p1[1]

    def straight_range(self):
        x0, y0 = self.p0
        x1, y1 = self.p1
        x_sign = 1 if x0 < x1 else -1
        y_sign = 1 if y0 < y1 else -1
        if x0 == x1:
            return ((x0, y) for y in range(y0, y1 + y_sign, y_sign))
        return ((x, y0) for x in range(x0, x1 + x_sign, x_sign))

    def diagonal_range(self):
        x0, y0 = self.p0
        x1, y1 = self.p1
        x_sign = 1 if x0 < x1 else -1
        y_sign = 1 if y0 < y1 else -1
        return zip(range(x0, x1 + x_sign, x_sign), range(y0, y1 + y_sign, y_sign))


def load():
    data = [line.strip() for line in open("input/day5.txt")]
    lines = []
    for d in data:
        p0, p1 = d.split(" -> ")
        lines.append(
            Line(tuple(map(int, p0.split(","))), tuple(map(int, p1.split(","))))
        )
    return lines


all_lines = load()

# Part 1
n_rows = max(max(l.p0[1], l.p1[1]) + 1 for l in all_lines)
n_cols = max(max(l.p0[0], l.p1[0]) + 1 for l in all_lines)

diagram = [[0 for _ in range(n_cols)] for _ in range(n_rows)]
for line in filter(lambda l: l.is_straight, all_lines):
    for x, y in line.straight_range():
        diagram[y][x] += 1
answer_1 = sum([sum(v > 1 for v in l) for l in diagram])

# Part 2
diagram = [[0 for _ in range(n_cols)] for _ in range(n_rows)]
for line in all_lines:
    if line.is_straight():
        for x, y in line.straight_range():
            diagram[y][x] += 1
    else:
        for x, y in line.diagonal_range():
            diagram[y][x] += 1
answer_2 = sum([sum(v > 1 for v in l) for l in diagram])
