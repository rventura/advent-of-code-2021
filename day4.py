def load() -> tuple[list[int], list[list[list[int]]]]:
    grids = []
    with open("input/day4.txt") as f:
        lines = [line.strip() for line in f.readlines()]
        draws = [int(i) for i in lines.pop(0).split(",")]
        _ = lines.pop(0)
        tmp_grid = []
        for line in lines:
            if line:
                tmp_grid.append([int(i) for i in line.split()])
            else:
                grids.append(tmp_grid)
                tmp_grid = []
    return draws, grids


def transpose_grid(array: list[list[int]]):
    n_cols = len(array[0])
    return [[line[i] for line in array] for i in range(n_cols)]


def winning_score(grid, draws) -> tuple[int, int]:
    transposed_grid = transpose_grid(grid)
    drawn = set()
    for step in range(len(draws)):
        last = draws.pop(0)
        drawn |= {last}
        all_comb = [set(line) for line in grid + transposed_grid]
        for comb in all_comb:
            if comb <= drawn:
                grid_values = set(el for line in grid for el in line)
                grid_sum = sum(grid_values - drawn)
                return step, last * grid_sum
    return -1, 0


all_draws, all_grids = load()
all_scores = [winning_score(grid, all_draws.copy()) for grid in all_grids]

min_step = len(all_draws) + 1
max_step = -1
for n_step, score in all_scores:
    if n_step < min_step:
        min_step = n_step
        quickest_score = score
    if n_step > max_step:
        max_step = n_step
        longest_score = score

answer_1 = quickest_score
answer_2 = longest_score
