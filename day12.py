from collections import Counter, defaultdict
from typing import Callable


def condition_1(history, candidate):
    return candidate.isupper() if candidate in history else True


def condition_2(history, candidate):
    if candidate.isupper() or candidate not in history:
        return True
    if candidate == "start" or any(
        count > 1 for count in Counter(filter(str.islower, history)).values()
    ):
        return False
    return True


def explore(
    history: list[str],
    adj: defaultdict,
    rule: Callable[[list[str], str], bool],
    paths: list[list[str]],
):
    """Explore graph given a history of vertices.

    Args:
      - history: The series of visited vertices.
      - adj: A mapping of vertices to their neighbors.
      - rule: A condition on visited points and candidate next vertex sentencing if
          exploration should continue.
      - paths: A list of explored complete paths.

    """
    neighbors = adj[history[-1]]
    for neighbor in neighbors:
        if neighbor == "end":
            paths.append(history + ["end"])
            continue
        if rule(history, neighbor):
            explore(history + [neighbor], adj, rule, paths)


adjacency = defaultdict(list)
lines = [line.strip() for line in open("input/day12.txt").readlines()]
for line in lines:
    v0, v1 = line.split("-")
    adjacency[v0].append(v1)
    adjacency[v1].append(v0)

# Parts 1 and 2
all_paths_1: list[list[str]] = []
all_paths_2: list[list[str]] = []
explore(["start"], adjacency, condition_1, all_paths_1)
explore(["start"], adjacency, condition_2, all_paths_2)
answer_1 = len(all_paths_1)
answer_2 = len(all_paths_2)
