instructions = [line.strip().split(" ") for line in open("input/day2.txt").readlines()]

# Part 1
position = [0, 0]

for direction, step in instructions:
    if direction == "forward":
        position[0] += int(step)
    elif direction == "down":
        position[1] += int(step)
    elif direction == "up":
        position[1] -= int(step)
    else:
        raise ValueError(f"Unknown direction {direction}")

answer_1 = position[0] * position[1]

# Part 2
position = [0, 0]
aim = 0

for direction, step in instructions:
    if direction == "forward":
        position[0] += int(step)
        position[1] += int(step) * aim
    elif direction == "down":
        aim += int(step)
    elif direction == "up":
        aim -= int(step)
    else:
        raise ValueError(f"Unknown direction {direction}")

answer_2 = position[0] * position[1]
