from collections import Counter, defaultdict
from functools import reduce

lines = [line.strip() for line in open("input/day14.txt").readlines()]
polymer_template = list(lines.pop(0))
insertion_map: dict[str, str] = {}

lines.pop(0)
for line in lines:
    p_pair, inserted = line.split(" -> ")
    insertion_map[p_pair] = inserted


def step(current_pairs: Counter[str]) -> Counter[str]:
    new_counters: list[Counter[str]] = []
    for pair, num in current_pairs.items():
        new_counters.append(
            Counter(
                {pair[0] + insertion_map[pair]: num, insertion_map[pair] + pair[1]: num}
            )
        )
    return reduce(lambda x, y: x + y, new_counters)


def count_elements(pair_counter: Counter[str]) -> int:
    double_counter: defaultdict[str, int] = defaultdict(int)
    for pair, count in pair_counter.items():
        double_counter[pair[0]] += count
        double_counter[pair[1]] += count
    element_counter = Counter(
        {elem: count // 2 for elem, count in double_counter.items()}
    )
    element_counter[polymer_template[0]] += 1
    element_counter[polymer_template[-1]] += 1
    return max(element_counter.values()) - min(element_counter.values())


# Parts 1 and 2
element_pairs = []

element_counter_40: defaultdict[str, int] = defaultdict(int)

element_pairs.append(
    Counter("".join(pair) for pair in zip(polymer_template[:-1], polymer_template[1:]))
)
for _ in range(40):
    element_pairs.append(step(element_pairs[-1]))

answer_1 = count_elements(element_pairs[10])
answer_2 = count_elements(element_pairs[40])
