coordinates: set[tuple[int, int]] = set()
folds: list[tuple[str, int]] = []

with open("input/day13.txt") as f:
    lines = [line.strip() for line in f.readlines()]
    empty_line_ix = lines.index("")
    for line in lines[:empty_line_ix]:
        x, y = line.split(",")
        coordinates.add((int(x), int(y)))
    for line in lines[empty_line_ix + 1 :]:
        data = line[max(line.find("x"), line.find("y")) :]
        axis, ix = data.split("=")
        folds.append((axis, int(ix)))


def map_coordinates(
    coords: tuple[int, int], fold_axis: str, fold_ix: int
) -> tuple[int, int]:
    if fold_axis == "x":
        return coords if coords[0] < fold_ix else (2 * fold_ix - coords[0], coords[1])
    if fold_axis == "y":
        return coords if coords[1] < fold_ix else (coords[0], 2 * fold_ix - coords[1])
    raise ValueError(f"Unknown axis value {fold_axis}")


# Part 1
one_folded_coords = set(map_coordinates(coords, *folds[0]) for coords in coordinates)
answer_1 = len(one_folded_coords)

# Part 2
folded_coords = set(coordinates.copy())
for fold in folds:
    folded_coords = set(map_coordinates(fcoords, *fold) for fcoords in folded_coords)

n_x = min(ix for axis, ix in folds if axis == "x")
n_y = min(ix for axis, ix in folds if axis == "y")

grid = [["." for _ in range(n_x)] for _ in range(n_y)]
for cx, cy in folded_coords:
    grid[cy][cx] = "#"

for gline in grid:
    print("".join(gline))
