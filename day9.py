from functools import reduce

from utils import cell_neighbors_ix

heights = [
    [int(i) for i in line.strip()] for line in open("input/day9.txt").readlines()
]
floor_shape = (len(heights), len(heights[0]))


def explore_basin(basin_points: list[tuple[int, int]]) -> list[tuple[int, int]]:
    reached = []
    for point in basin_points:
        i, j = point
        neighbors_ix = cell_neighbors_ix(point, floor_shape, False)
        for nix in neighbors_ix:
            neigh_height = heights[nix[0]][nix[1]]
            if heights[i][j] < neigh_height < 9:
                reached.append(nix)
    return reached


# Part 1
low_points_ix = []

for i in range(floor_shape[0]):
    for j in range(floor_shape[1]):
        neigh_ix = cell_neighbors_ix((i, j), floor_shape, False)
        if all(heights[i][j] < heights[nix[0]][nix[1]] for nix in neigh_ix):
            low_points_ix.append((i, j))
answer_1 = sum(heights[i][j] + 1 for i, j in low_points_ix)

# Part 2
basins = {(i, j): [(i, j)] for i, j in low_points_ix}
for basin_points in basins.values():
    while True:
        reached_points = explore_basin(basin_points)
        new_points = list(filter(lambda p: p not in basin_points, reached_points))
        new_points = list(set(new_points))
        if new_points:
            basin_points.extend(new_points)
        else:
            break

sorted_bassins = sorted(basins.values(), key=len)
answer_2 = reduce(lambda x, y: x * y, [len(b) for b in sorted_bassins[-3:]])
