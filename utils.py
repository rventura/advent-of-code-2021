from functools import reduce
from typing import Callable, TypeVar

T = TypeVar("T")


def apply(fun: Callable, args: T, n: int):
    """Apply a function n times on some given argument."""
    return reduce(lambda x, _: fun(x), range(n), args)


def cell_neighbors_ix(
    ix: tuple[int, int], grid_shape: tuple[int, int], include_diag: bool = False
) -> list[tuple[int, int]]:
    n, m = grid_shape
    i, j = ix

    def boundary_filter(coord: tuple[int, int]) -> bool:
        if coord[0] < 0 or coord[0] >= n:
            return False
        if coord[1] < 0 or coord[1] >= m:
            return False
        return True

    neighbors_ix = [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)]
    if include_diag:
        neighbors_ix += [(i - 1, j - 1), (i + 1, j - 1), (i - 1, j + 1), (i + 1, j + 1)]

    return list(filter(boundary_filter, neighbors_ix))
