import numpy as np

with open("input/day1.txt") as f:
    depths = np.array([int(i) for i in f.read().split("\n")[:-1]])

n_increase = sum(np.diff(depths) > 0)
depth_groups = np.array(
    [
        depths[i] + depths[j] + depths[k]
        for i, j, k in zip(
            range(len(depths)), range(1, len(depths) - 1), range(2, len(depths))
        )
    ]
)
n_groups_increase = sum(np.diff(depth_groups) > 0)
